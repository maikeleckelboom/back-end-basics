<?php

$marks = array(
    "maikel" => array(
        "naam" => "Maikel",
        "nederlands" => 7.5,
        "engels" => 8.0,
        "rekenen" => 9.2,
        "programmeren" => 8.8,
        "databases" => 7.6,
    ),

    "kim" => array(
        "naam" => "Kim",
        "nederlands" => 6.5,
        "engels" => 7.0,
        "rekenen" => 8.2,
        "programmeren" => 6.8,
        "databases" => 6.6,
    ),
);

// Cijfers van Maikel uit array exclusief naam
(array_slice($marks['maikel'],1)) ;
$count_maikel = array_sum(array_slice($marks['maikel'],1));

// Cijfers van Kim uit array exclusief naam
(array_slice($marks['kim'],1)) ;
$count_kim = array_sum(array_slice($marks['kim'],1));

// Tel items van één student
$countSingle = count(array_slice($marks['maikel'], 1));

// Tel items van alle studenten
$countItems = count(array_slice($marks['maikel'], 1)) + count(array_slice($marks['kim'], 1));

// Bereken totaal opgetelde cijfers
$countTotal = $count_kim + $count_maikel;

// Bereken gemiddeld voor Maikel
$gemiddeld_maikel = $count_maikel / $countSingle;

// Bereken gemiddeld voor Kim
$gemiddeld_kim = $count_kim / $countSingle;

// Bereken gemiddeld voor Groep
$gemiddeld_groep = $countTotal / $countItems;

// Voeg opnieuw gemiddelde waardes toe aan de array
//array_push($marks, $gemiddeld_maikel, $gemiddeld_kim, $gemiddeld_groep);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../css/app.css">
    <title>Backend Basics</title>
</head>
<body class="les_01">

<table class="rapport-table">
    <thead>
    <tr>
        <th>Naam</th>
        <th>Nederlands</th>
        <th>Engels</th>
        <th>Rekenen</th>
        <th>Programmeren</th>
        <th>Databases</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($marks as $rows) : ?>
        <tr>
            <td><?= $rows['naam']; ?></td>
            <td><?= $rows['nederlands']; ?></td>
            <td><?= $rows['engels']; ?></td>
            <td><?= $rows['rekenen']; ?></td>
            <td><?= $rows['programmeren']; ?></td>
            <td><?= $rows['databases']; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<table class="rapport-table">
    <tr>
        <th>Gemiddeld Groep</th>
        <th>Gemiddeld Maikel</th>
        <th>Gemiddeld Kim</th>
    </tr>
    <tr>
        <td><?= $gemiddeld_groep ?></td>
        <td><?= $gemiddeld_maikel ?></td>
        <td><?= $gemiddeld_kim ?></td>
    </tr>
</table>

</body>
</html>