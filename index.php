<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/app.css">
    <title>Backend Basics</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            flex-flow: column wrap;
        }

        h1 {
            margin: 8px 0;
        }

        img[alt="Directory"] {
            height: 32px;
            margin: 8px;
            position: relative;
            top: 12px;
        }
    </style>
</head>
<body>

<h1><img src="files/folder.svg" alt="Directory">Backend Basics</h1>
<h3>Xampp TECH-STACK</h3>
<a href="hoofdstuk_01"><button class="button"><span>Alle opdrachten</span></button></a>


<script src="js/ripple.js"></script>
</body>
</html>