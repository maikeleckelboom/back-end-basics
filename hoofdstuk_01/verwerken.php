<?php

if (isset($_POST["submit"])) {
    if (empty($_POST["naam"])) {
        echo "Naam niet ingevuld";
    } else {
        $naam = $_POST['naam'];
        $taal = $_POST['taal'];
        if ($taal == "N") {
            echo "Goedendag " . $naam;
        } elseif ($taal == "E") {
            echo "Good morning " . $naam;
        } elseif ($taal == "S") {
            echo "Buenos dias " . $naam;
        } else {
            echo "Taal niet ingevuld";
        }
    }

};

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Backend Basics | $_POST-variabelen | Verwerken</title>
    <style>

    </style>
</head>
<body>

<h1>$_POST-variabelen | Verwerken</h1>

<form action="" method="post">
    <label for="naam">Uw naam</label>
    <input type="text" name="naam" id="naam" placeholder="Naam" title="">
    <input type="text" name="postcode" id="postcode" placeholder="Postcode" title="">
    <input type="text" name="straat" id="straat" placeholder="Straat" title="">
    <input type="text" name="Plaats" id="plaats" placeholder="Plaats" title="">
    <input type="email" name="e-mail" id="e-mail" placeholder="E-mailadres" title="">
    <textarea name="commentaar" placeholder="Typ hier je commentaar in ..." rows="4" title=""></textarea>

    <input type="submit" name="submit" value="Versturen">
</form>


</body>
</html>