<?php

// Opgave 25
$gewerkteuren = 40;
$uurtarief = 15.00;
$bonus = 100.00;
$bruto = $gewerkteuren * $uurtarief;
if($gewerkteuren <= 40) {
    echo "Uw basissalaris is: € " . $bruto;
    echo "<br>Uw belasting is: €" . 0.40 * $bruto;
}

echo "<br>";

// If else if statement
$uur = 10;
if($uur <= 11) {
    echo "Goedemorgen";
}
elseif($uur >= 12 && $uur < 18) {
    echo "Goedemiddag";
}
else {
    echo "Goedenavond";
}

echo "<br>";


// Opgave 26
$gewerkteuren = 45;
$salarisBruto = $gewerkteuren * $uurtarief;

if($gewerkteuren <= 40) {
    echo "<br> Uw basissalaris is: €" . $salarisBruto;
    echo "<br> Uw basissalaris plus bonus is: €" . $salarisBruto;
    echo "<br> Uw belastig is: €" . 0.45 * $salarisBruto;
}
elseif($gewerkteuren >= 40) {
    echo "<br> Uw basissalaris is: €" . $salarisBruto;
    echo "<br> Uw basissalaris plus bonus is: €" . ($salarisBruto + $bonus);
    echo "<br> Uw belastig is: €" . 0.45 * ($salarisBruto + $bonus);
    echo "<br> Uw nettobedrag is: €" . ($salarisBruto - 0.21);
}


