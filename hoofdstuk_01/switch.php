<!DOCTYPE html>
<html lang="nl">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Inconsolata:400,700&display=swap');

        body {
            font-size: 9px;
            font-family: 'Inconsolata', monospace;
        }

        .album {
            clear: left;
            width: 100%;
        }

        .omslag {
            float: left;
        }

        .gegevens {
            float: left;
            padding-left: 20px;
        }

        .korting {
            clear: left;
        }

        .aantal {
            background: #f8ce6c;
        }

        img {
            height: 72px;
        }

        .winkelmand-container {
            max-width: 400px;
            margin: 0 auto;
            margin-top: 200px;
            background: rgba(228, 228, 228, 0.24);
            padding: 20px;
        }

    </style>
    <title>Winkelmandje</title>
</head>
<body>

<div class="winkelmand-container">
    <h3>Switch</h3>

    <?php
    if (isset($_POST['land'])) {
        switch ($_POST['land']) {
            case "nl" :
                echo "<p>Retourtje Nedrland is 100</p>";
                break;
            case "be" :
                echo "<p>Retourtje België is 80</p>";
                break;
            case "de" :
                echo "<p>Retourtje Duitsland is 90</p>";
                break;
            case "es" :
                echo "<p>Retourtje Spanje is 80</p>";
                break;
            default:
                echo "<p>U heeft geen bestemming gekozen</p>";
        }
    };
    ?>

    <form name="order" action="" method="post">
        <p>Selecteer een bestemming</p>
        <select name="land" value="true">
            <option value=" "></option>
            <option value="nl">Nederland</option>
            <option value="be">België</option>
            <option value="de">Duitsland</option>
            <option value="es">Spanje</option>
        </select>
        <input type="submit" name="submit" value="Versturen">
    </form>
</div>

<!--
    -> Kennis test


-->

</body>
</html>