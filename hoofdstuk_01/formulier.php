<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--    <link rel="stylesheet" type="text/css" href="../css/app.css">-->
    <title>Backend Basics | $_POST-variabelen</title>
    <style>

    </style>
</head>
<body>

<h1>$_POST-variabelen</h1>

<form action="verwerken.php" method="post">
    <label for="naam">Uw naam</label>
    <input type="text" name="naam" title="" id="naam">
    <input type="hidden" name="taal" value="false" title="">
    <br>Kies een taal:
    <input type="radio" name="taal" value="N" title="" id="nederlands">
    <label for="nederlands">Nederlands</label>
    <input type="radio" name="taal" value="E" title="" id="engels">
    <label for="engels">Engels</label>
    <input type="radio" name="taal" value="S" title="" id="spaans">
    <label for="spaans">Spaans</label>
    <br>
    <input type="submit" name="submit" value="Versturen">

</form>


</body>
</html>