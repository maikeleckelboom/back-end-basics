<?php

// Opgave 58 - formatteren
$format = '%A %d %B %Y %H:%M%S';
$geformatteerde_datum = strftime($format);
echo "<br>Geformateerde datum: " . $geformatteerde_datum;

// Opgave 59 - lokale datuminstelling
setlocale(LC_TIME, "nld_NLD");
$ned = strftime($format);
echo "<br>In het Nederlands: " . $ned;

// Opgave 60 - date_format verwerken
$datum = date_create("2000-01-01");
echo "<br>Datum formatteren: " . date_format($datum, "Y-m-d");
echo "<br>Datum formatteren: " . date_format($datum, "d-m-Y");

// Opgave 61 - date_diff
$factuurdatum = date_create("2018-10-10");
echo "<br>Fatuurdatum: " . date_format($factuurdatum, "Y-m-d");
$vandaag = date_create("Now");
echo "<br>Vandaag: " . date_format($vandaag, "Y-m-d");
$interval = date_diff($vandaag, $factuurdatum);
echo "<br>U heeft nog: " .$interval->format("%R%a dagen om te betalen");