<?php

// Super globale scope
$GLOBALS["url"] = "www.mijndomeinnaam.nl";

// Globale Scope
global $email;
$email = "webmaster@mijndomeinnaam.nl";

// Globale constanten
define("BIJDRAGE", 0.10);

function doneren($bedrag) {
    // Function scope
    $melding = "GIRO 555";
    echo "<br>" . $melding;
    echo "<br> URL " . $GLOBALS["url"];
    echo "<br>Bedrag: " . $bedrag;
    global $email;
    echo "<br>E-mail:" . $email;
    $bijdrage = $bedrag * BIJDRAGE;
    $donatie = $bedrag + $bijdrage;
    echo "<br>Inclusief bijdrage:" . $donatie;
    static $pot;
    $pot = $pot + $donatie;
    echo "<br><span style='background: yellow;'>Totaal bedrag in pot $pot</span><br>";
}
doneren(300);
doneren(1000);
doneren(33333);