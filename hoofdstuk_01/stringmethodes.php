<?php

if (isset($_POST["submit"])) {

    if (empty($_POST["naam"])) {
        echo "Naam niet ingevuld";
    } else if (empty($_POST["straat"])) {
        echo "Straat niet ingevuld";
    } else if (empty($_POST["postcode"])) {
        echo "Postcode niet ingevuld";
    } else if (empty($_POST["plaats"])) {
        echo "Plaats niet ingevuld";
    } else if (empty($_POST["e-mail"])) {
        echo "E-mail niet ingevuld";
    } else if (empty($_POST["commentaar"])) {
        echo "Commentaar niet ingevuld";
    } else {

        $naam = htmlspecialchars($_POST['naam']);
        $straat = htmlspecialchars($_POST['straat']);
        $postcode = htmlspecialchars($_POST['postcode']);
        $plaats = htmlspecialchars($_POST['plaats']);
        $email = htmlspecialchars($_POST['e-mail']);
        $commentaar = htmlspecialchars($_POST['commentaar']);
    }
};

$naam = trim($naam);
$straat = trim($straat);
$postcode = trim($postcode);
$plaats = trim($plaats);
$email = trim($email);

$plaats = strtoupper($plaats);

if ($plaats == "AMSTERDAM") {
    $bezorgkosten = 10.00;
} elseif ($plaats == "UTRECHT") {
    $bezorgkosten = 20.00;
} else {
    $bezorgkosten = 30.00;
}

echo "<br>Bezorgkosten: $bezorgkosten";


// Opgave 35
$email = strtolower($email);
echo "<br>E-mailadress: $email";


// Opgave 36
$naam = ucfirst($naam);
echo "<br>Naam: $naam";


// Opgave 37
$emailarray = explode("@", $email);
$user = $emailarray[0];
$domein = $emailarray[1];
echo "<br>User: " . $user;
echo "<br>Domein: " . $domein;


// Opgave 38
if (strlen($postcode) != 7)
    echo '<br> Postcode incorrect ingevuld';


// Opgave 39
$postcodePrefix = substr($postcode, 0, 4);
$postcodeSuffix = substr($postcode, 5, 2);
echo "<br> Postcode prefix: $postcodePrefix";
echo "<br> Postcode suffix: $postcodeSuffix";


/// Opgave 40
$nl = strpos($email, ".nl");
$be = strpos($email, ".be");
$fr = strpos($email, ".fr");

if ($nl > 0) echo "<br> Nederlands e-mailadres";
if ($be > 0) echo "<br>Belgisch e-mailadres";
if ($fr > 0) echo "<br>Frans e-mailadres";


// Opgave 41
$commentaar = nl2br($commentaar);

// Opgave 42
$scheldwoorden = array("debiel", "laf", "gestoord");
str_replace($scheldwoorden, "#@*!%!", $commentaar);

echo "Commentaar: $commentaar";


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../css/app.css">
    <title>Backend Basics | $_POST-variabelen</title>
    <style>

        * {
            box-sizing: border-box;
        }

        h1 {
            text-align: center;
            margin: 24px 0;
        }

        form {
            max-width: 400px;
            margin: 0 auto;
        }

        form input, form label {
            height: 38px;
            margin: 4px 0;
            font-size: 16px;
        }

        form input {
            padding: 8px;
        }

        form input[type="radio"] {
            margin: 4px;
            cursor: pointer;
        }

        form label {
            font-size: 18px;
            line-height: 2;
            position: relative;
            top: -14px;
            margin-right: 8px;
            cursor: pointer;
        }

        .left-side {
            width: 120px;
        }

        .right-side {
            width: 280px;
        }

        .left-side, .right-side {
            display: flex;
            flex-direction: column;
        }

        .sides {
            display: flex;
        }

        .buttons {
            justify-content: flex-end;
            display: flex;
        }

        .buttons input[type="submit"],
        .buttons input[type="reset"] {
            padding: 12px 18px;
            cursor: pointer;
            border-radius: 2px;
            border: solid 1px #cdcdcd;
            line-height: 0;
        }

        .buttons input[type="submit"] {
            background: #2a49a5;
            color: white;
        }

        .buttons input[type="reset"] {
            background: #88d4ff;
        }

        .buttons input:nth-child(1) {
            margin-right: 8px;
        }

        textarea {
            width: 100%;
            padding: 8px;
        }

    </style>
</head>
<body>

<h1>String Methodes</h1>

<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
    <div class="sides">
        <div class="left-side">
            <label for="naam">Naam</label>
            <label for="straat">Straat</label>
            <label for="postcode">Postcode</label>
            <label for="plaats">Plaats</label>
            <label for="e-mail">E-mailadres</label>
        </div>
        <div class="right-side">
            <input type="text" name="naam" id="naam" placeholder="Naam" title="">
            <input type="text" name="straat" id="straat" placeholder="Straat" title="">
            <input type="text" name="postcode" id="postcode" placeholder="Postcode" title="">
            <input type="text" name="plaats" id="plaats" placeholder="Plaats" title="">
            <input type="email" name="e-mail" id="e-mail" placeholder="E-mailadres" title="">
        </div>
    </div>

    <div class="bottom">
        <textarea placeholder="Typ hier je bericht" name="commentaar" title="" rows="4"></textarea>

        <div class="buttons">
            <input type="reset" name="reset" value="Reset">
            <input type="submit" name="submit" value="Versturen">
        </div>
    </div>
</form>


</body>
</html>