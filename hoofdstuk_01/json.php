<?php

// Opgave 65
$user = array(
    "naam" => "Shyam",
    "e-mail" => "shyan@nu.nl",
    "wachtwoord" => "",
    "photo" => "shyam.jpg"
);

echo "<br>userArray: <br>";
var_dump($user, true);
$userJsonString = json_encode($user);
echo "<br>userJsonLiteral: <br>" . $userJsonString;


// Opgave 66
$boekenJsonLitral = '[
    {"titel":"Stoner","ateur":"John Williams","genre":"Fictie","prijs":"19.99"},
    {"titel":"De cirkel","ateur":"Dave Eggers","genre":"Fictie","prijs":"22.5"},
    {"titel":"Rayuela","ateur":"Julio Cortazar","genre":"Fictie","prijs":"25.5"}
]';


// Array
$boekenObj = json_decode($boekenJsonLitral,true);
foreach($boekenObj as $boek) {
    echo "<br>Titel: " . $boek['titel'];
    echo "<br>Ateur: " . $boek['ateur'];
    echo "<br>Genre: " . $boek['genre'];
    echo "<br>Prijs: " . $boek['prijs'];
}

// Objecten
$boekenObj = json_decode($boekenJsonLitral);
foreach($boekenObj as $boek) {
    echo "<br>Titel: " . $boek->titel;
    echo "<br>Ateur: " . $boek->ateur;
    echo "<br>Genre: " . $boek->genre;
    echo "<br>Prijs: " . $boek->prijs;
}