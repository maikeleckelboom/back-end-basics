<?php

$prijs = 9;

if (isset($_POST["submit"])) {
    $aantal = $_POST["aantal"];
    $aantal = implode(" ", $aantal);
    $totaal = $aantal * $prijs;
}


if (isset($_POST["klant"], $_POST["student"])) {
    $kortingspercentage = ($totaal / 100) * 25;
    $resultaat = $totaal - $kortingspercentage . "<br>";

} elseif (isset($_POST["student"])) {
    $kortingspercentage = ($totaal / 100) * 15;
    $resultaat = $totaal - $kortingspercentage . "<br>";

} elseif (isset($_POST["klant"])) {
    $kortingspercentage = ($totaal / 100) * 10;
    $resultaat = $totaal - $kortingspercentage . "<br>";
}


?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Inconsolata:400,700&display=swap');

        body {
            font-size: 16px;
            font-family: 'Inconsolata', monospace;
        }

        .album {
            clear: left;
            width: 100%;
        }

        .omslag {
            float: left;
        }

        .gegevens {
            float: left;
            padding-left: 20px;
        }

        .korting {
            clear: left;
        }

        .aantal {
            background: #f8ce6c;
        }

        img {
            height: 72px;
        }

        .winkelmand-container {
            max-width: 600px;
            margin: 0 auto;
            margin-top: 200px;
            background: rgba(228, 228, 228, 0.24);
            padding: 20px;
        }

    </style>
    <title>Winkelmandje</title>
</head>
<body>

<div class="winkelmand-container">
    <h3>Mijn Winkelmandje</h3>

    <form name="albums" action="" method="post">

        <div class="album">
            <div class="omslag">
                <img src="../../files/shopping-cart.svg" alt="">
            </div>
            <div class="gegevens">
                <p>Cesaria Evora "Em Um Concerto"</p>

                <p>
                    <?php

                    echo "Prijs per stuk is €" . $prijs . "<br>";

                    if (isset($totaal)) {
                        echo "Prijs totaal excl. korting is: €" . $totaal . "<br>";
                    }

                    if (isset($resultaat)) {
                        echo "Totaal bedrag incl korting is: €" . $resultaat . "<br>";
                    }

                    if (isset($aantal)) {
                        echo "Het aantal items is: " . $aantal . "<br>";
                    }

                    ?>
                </p>
                <input type="hidden" name="albumcode[0]" value="001">
                <input type="hidden" name="artiest[0]" value="Cesaria Evora">
                <input type="hidden" name="album[0]" value="Em Um Concerto">
                <input type="hidden" name="prijs[0]" value="9">
                <input type="hidden" name="genre[0]" value="World"> <br>
                <input type="text" size="2" maxlength="3" name="aantal[0]" class="aantal" value="0" title="">
            </div>
        </div>


        <div class="korting">
            <br>
            <hr>
            Korting:<br>
            <input type="checkbox" name="student" value="15" title=""> Student 15% <br>
            <input type="checkbox" name="klant" value="10" title=""> Klant 10% <br>
            <input type="submit" width="300px" name="submit" value="Bestellen">
        </div>

    </form>
</div>

<!--
    1. Wat doet het attribuut action=""?
    Doorverwijzen naar de pagina waar evt. de data verwerkt wordt.
    2. wat doet het attribuut type="hidden"?
    Verbergt het input element.
    3. Wat doet de index in albumcode[0]
    Hier geef je een key mee voor het maken van een array
    4. Wat is het nut van het indexeren van html elementen?
    Zodat je met php onderscheid kan maken.
    5. Hoe onderscheid je checkbox-elementen?
    Met het name attribuut en een id
    7. Mogen bij all echeckbox-elementen een vink mogen worden geplaatst?
    ja
    8. Wat is het datatype van het resultaat van de isset-methode?
    Boolean
    9. Hoe controleer je of albumcode ingevuld is?
    Check de staat van de checkbox of het checked is.
    10. Wanneer is het resultaat van isset false?
    Als de input niet is ingevuld.
-->

</body>
</html>