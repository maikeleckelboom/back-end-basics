<?php

// Stap 1:
// Input formulier met voornaam, naam, geboortedatum, straat, postcode, plaats, email, wachtwoord

// Stap 2:
// Associatieve array $gebruiker voor de gegevens uit het formulier. Het resultat van print_r($gebruiker) moet zijn -> Zie boek.

// Stap 3:
// Maak een string met json_encode($gebruiker) resultaat -> zie boek

?>

<form>
    <div class="right-side">
        <input type="text" name="naam" id="naam" placeholder="Naam" title="">
        <input type="text" name="straat" id="straat" placeholder="Straat" title="">
        <input type="text" name="postcode" id="postcode" placeholder="Postcode" title="">
        <input type="text" name="plaats" id="plaats" placeholder="Plaats" title="">
        <input type="email" name="e-mail" id="e-mail" placeholder="E-mailadres" title="">
    </div>
</form>
