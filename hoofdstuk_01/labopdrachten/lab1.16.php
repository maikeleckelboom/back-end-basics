<!DOCTYPE html>
<html lang="nl">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <script src="../../js/jquery.js"></script>
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/app.css">
    <title>Winkelmandje</title>
</head>
<body class="lab_16">

<div class="winkelmand-container">

    <div class="callout">
        <h3><img src="../../files/shopping-cart.svg" alt="Winklmandje">Mijn Winkelmandje</h3>
    </div>

    <form name="albums" action="" method="post">

        <div class="albums-container">
            <!-- Album 1 -->
            <div class="album">
                <div class="omslag">
                    <img src="../../files/album1.jpg" alt="">
                </div>
                <div class="gegevens">
                    <h4>Cesaria Evora "Em Um Concerto"</h4>
                    <p>Prijs: €9,-</p>

                    <input type="hidden" name="albumcode[0]" value="001">
                    <input type="hidden" name="artiest[0]" value="Cesaria Evora">
                    <input type="hidden" name="album[0]" value="Em Um Concerto">
                    <input type="hidden" name="prijs[0]" value="9">
                    <input type="hidden" name="genre[0]" value="World"><br>

                    <label for="aantal[0]">Aantal:</label>
                    <div class="quantity">
                        <input type="number" min="1" max="999" step="1" value="1" title="" name="aantal[0]"
                               id="aantal[0]">
                    </div>

                </div>
            </div>


            <!-- Album 2 -->
            <div class="album">
                <div class="omslag">
                    <img src="../../files/album2.jpg" alt="">
                </div>
                <div class="gegevens">
                    <h4>Manu Chao "Clandestino"</h4>
                    <p>Prijs: €5,-</p>

                    <input type="hidden" name="albumcode[1]" value="002">
                    <input type="hidden" name="artiest[1]" value="Manu Chao">
                    <input type="hidden" name="album[1]" value="Clandstino">
                    <input type="hidden" name="prijs[1]" value="5">
                    <input type="hidden" name="genre[1]" value="World"><br>

                    <label for="aantal[1]">Aantal:</label>
                    <div class="quantity">
                        <input type="number" min="1" max="999" step="1" value="1" title="" name="aantal[1]"
                               id="aantal[1]">
                    </div>

                </div>
            </div>


            <!-- Album 3 -->
            <div class="album">
                <div class="omslag">
                    <img src="../../files/album3.jpg" alt="">
                </div>
                <div class="gegevens">
                    <h4>Joopie Hoopie"</h4>
                    <p>Prijs: €12</p>
                    <input type="hidden" name="albumcode[2]" value="003">
                    <input type="hidden" name="artiest[2]" value="Joopie Hoopie">
                    <input type="hidden" name="album[2]" value="Jeuupie">
                    <input type="hidden" name="prijs[2]" value="15">
                    <input type="hidden" name="genre[2]" value="Rock"><br>
                    <label for="aantal[2]">Aantal:</label>

                    <div class="quantity">
                        <input type="number" min="1" max="999" step="1" value="1" title="" name="aantal[2]"
                               id="aantal[2]">
                    </div>

                </div>
            </div>
        </div>


        <div class="actions-wrapper">

            <div class="select-container">
                <label class="box">
                    <span>Selecteer een betalingswijze</span>
                    <select name="betalingswijze">
                        <option value="visa">Visa</option>
                        <option value="mastercard">MasterCard</option>
                        <option value="paypal">PayPal</option>
                        <option value="ideal">Ideal</option>
                    </select>
                </label>
            </div>

            <div class="korting-container">
                <p>Kies je korting</p>
                <input type="checkbox" name="student" value="15" title="" class="checkbox-custom " id="student">
                <label for="student" class="checkbox-custom-label">Student 15%</label> <br>
                <input type="checkbox" id="klant" name="klant" value="10" title="" class="checkbox-custom">
                <label for="klant" class="checkbox-custom-label">Klant 10%</label>
            </div>

            <div class="verzenden-container">
                <input type="submit" width="300px" name="verzenden" value="Bestellen">
            </div>

        </div>
    </form>

    <div class="callout">
        <?php

        // Als verzenden gezet is in de $_POST variabele
        if (isset($_POST['verzenden'])) {
            include_once("externe_functions.php");
            $betalingswijze = $_POST['betalingswijze'];
            $serviceKosten = serviceKosten();
            $aantal = $_POST["aantal"];
            $aantal = implode(", ", $aantal);
        }

        // Open table tag
        echo "<table class='data-table'>";

        // Totaal aantal producten
        if (isset($aantal)) {
            echo "<tr><td>De aantallen zijn:</td> " . "<td>" . $aantal . "</td>";
        }

        // Kortings percentage
        if (isset($_POST['verzenden'])) {
            echo "<tr><td>De korting is: </td><td>" . korting() . "% </td></tr>";
        }

        // Switch case
        if (isset($betalingswijze)) {
            switch ($_POST['betalingswijze']) {
                case "visa" :
                    $method = "Visa";
                    break;
                case "mastercard" :
                    $method = "MasterCard";
                    break;
                case "paypal" :
                    $method = "PayPal";
                    break;
                case "ideal" :
                    $method = "Ideal";
                    break;
                default:
                    echo "Nog geen betaalmethode gekozen";
            }
            echo "<tr><td>Betaal methode is: </td><td>" . $method . "</td></tr>";
        } else {
            echo "Nog geen betaalmethode gekozen";
        }

        // Sluit table tag
        echo "</table>";

        ?>
    </div>

    <!-- Facaturing -->
    <div class="factuur">
        <?php if (isset($_POST['verzenden'])) { ?>
            <h2>Factuur</h2>
            <div class="table-container">
                <table>
                    <?php facturering(); ?>
                </table>
            </div>
            <?php premium();
        } ?>
    </div>

</div>
<script src="../../js/number.js"></script>
<script src="../../js/ripple.js"></script>
</body>
</html>