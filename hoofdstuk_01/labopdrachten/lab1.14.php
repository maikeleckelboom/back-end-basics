<!DOCTYPE html>
<html lang="nl">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../css/winkelmandje.css">
    <title>Winkelmandje</title>
</head>
<body>

<div class="winkelmand-container">
    <div class="callout">
        <h3>Mijn Winkelmandje</h3>
    </div>

    <form name="albums" action="" method="post">

        <!-- Album 1 -->
        <div class="album">
            <div class="omslag">
                <img src="../../files/album1.jpg" alt="">
            </div>
            <div class="gegevens">
                <h4>Cesaria Evora "Em Um Concerto"</h4>
                <p>Prijs: €9,-</p>

                <input type="hidden" name="albumcode[0]" value="001">
                <input type="hidden" name="artiest[0]" value="Cesaria Evora">
                <input type="hidden" name="album[0]" value="Em Um Concerto">
                <input type="hidden" name="prijs[0]" value="9">
                <input type="hidden" name="genre[0]" value="World"> <br>
                <label for="aantal_1">Aantal:</label>
                <input type="number" name="aantal[0]" size="2" maxlength="3" id="aantal_1" value="0" title="">
            </div>
        </div>


        <!-- Album 2 -->
        <div class="album">
            <div class="omslag">
                <img src="../../files/album2.jpg" alt="">
            </div>
            <div class="gegevens">
                <h4>Manu Chao "Clandestino"</h4>
                <p>Prijs: €5,-</p>

                <input type="hidden" name="albumcode[1]" value="002">
                <input type="hidden" name="artiest[1]" value="Manu Chao">
                <input type="hidden" name="album[1]" value="Clandstino">
                <input type="hidden" name="prijs[1]" value="5">
                <input type="hidden" name="genre[1]" value="World"><br>
                <label for="aantal_2">Aantal:</label>
                <input type="number" name="aantal[2]" size="2" maxlength="3" class="aantal" value="0" title="">
            </div>
        </div>


        <!-- Album 3 -->
        <div class="album">
            <div class="omslag">
                <img src="../../files/album3.jpg" alt="">
            </div>
            <div class="gegevens">
                <h4>Joopie Hoopie"</h4>
                <p>Prijs: €12</p>
                <input type="hidden" name="albumcode[2]" value="003">
                <input type="hidden" name="artiest[2]" value="Joopie Hoopie">
                <input type="hidden" name="album[2]" value="Jeuupie">
                <input type="hidden" name="prijs[2]" value="15">
                <input type="hidden" name="genre[2]" value="Rock"><br>
                <label for="aantal_3">Aantal:</label>
                <input name="aantal[3]" type="number" size="2" maxlength="3" value="0" title="">
            </div>
        </div>


        <div class="korting">

            <label>
                <span>Selecteer een betalingswijze:</span>
                <select name="betalingswijze">
                    <option value="visa">Visa</option>
                    <option value="mastercard">MasterCard</option>
                    <option value="paypal">PayPal</option>
                    <option value="ideal">Ideal</option>
                </select>
            </label>

            <p>Korting:</p>
            <input type="checkbox" name="student" value="15" title="" id="student">
            <label for="student">Student 15%</label> <br>
            <input type="checkbox" id="klant" name="klant" value="10" title="">
            <label for="klant">Klant 10%</label>
            <input type="submit" width="300px" name="verzenden" value="Bestellen">
        </div>

    </form>

    <div class="callout">
        <?php

        if (isset($_POST['verzenden'])) {
            include_once("externe_functions.php");
            $betalingswijze = $_POST['betalingswijze'];
            $serviceKosten = serviceKosten($betalingswijze);
            $aantal = $_POST["aantal"];
            $aantal = implode(" ", $aantal);
        }


        // Totaal aantal producten
        if (isset($aantal)) {
            echo "Het aantal is: " . $aantal . "<br>";
        } else {
            echo "";
        }


        // Kortings percentage
        if (isset($_POST['verzenden'])) {
            echo "De korting is: " . korting() . "% <br>";
        }


        // Switch case
        if (isset($betalingswijze)) {
            switch ($_POST['betalingswijze']) {
                case "visa" :
                    $method = "Visa";
                    break;
                case "mastercard" :
                    $method = "MasterCard";
                    break;
                case "paypal" :
                    $method = "PayPal";
                    break;
                case "ideal" :
                    $method = "Ideal";
                    break;
                default:
                    echo "Nog geen betaalmethode gekozen";
            }
            echo "Betaal methode is: " . $method . "<br>";
        } else {
            echo "Nog geen betaalmethode gekozen";
        }


        ?>
    </div>

</div>

<script src="../../js/ripple.js"></script>
</body>
</html>