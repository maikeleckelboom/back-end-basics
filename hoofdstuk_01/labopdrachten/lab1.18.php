<?php

// Super globale scope
$GLOBALS["url"] = "www.mijndomeinnaam.nl";

// Globale Scope
global $email;
$email = "webmaster@mijndomeinnaam.nl";

// Globale constanten
define("BIJDRAGE", 0.10);

function doneren($bedrag) {
    // Function scope
    $melding = "GIRO 555";
    echo "<br>" . $melding;
    echo "<br> URL " . $GLOBALS["url"];
    echo "<br>Bedrag: " . $bedrag;

    global $email;
    echo "<br>E-mail:" . $email;
    $bijdrage = $bedrag * BIJDRAGE;
    $donatie = $bedrag + $bijdrage;

    // Statische variable hoogste
    static $hoogste;

    // Hoogste berekening
    $hoogste = $donatie + $hoogste - $bedrag;

    // Echo de donatie bijdrage
    echo "<br>Inclusief bijdrage:" . $donatie;

    // Pot met totale bedrag
    static $pot;

    // Voeg hier bedrag van donatie aan toe
    $pot = $pot + $donatie;

    // Totale bedrag in de pot
    echo "<br><span style='background: yellow;'>Totaal bedrag in pot $pot</span><br>";

    // Hoogste donatie
    echo "<p>Hoogste donatie tot nu toe is: $hoogste</p>";
}

// Functie doneren aanroepen
doneren(100);
doneren(1000);
doneren(33333);