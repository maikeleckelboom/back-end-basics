<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../css/app.css">
    <title>Backend Basics | $_POST-variabelen</title>
</head>
<body>

<?php
    $voornaam = $_POST['voornaam'];
    $achternaam = $_POST['achternaam'];
    $straat = $_POST['straat'];
    $postcode = $_POST['postcode'];
    $plaats = $_POST['plaats'];
    $email = $_POST['e-mail'];
    $vakken = $_POST['vakken'];

    if (isset($_POST["submit"])) {
        if (empty($voornaam)) {
            echo "Voornaam niet ingevuld";
        } else if
        (empty($achternaam)) {
            echo "Achternaam niet ingevuld";
        } else if ($straat) {
            echo "Straat niet ingevuld";
        } else if
        (empty($straat)) {
            echo "Postcode niet ingevuld";
        } else if
        (empty($plaats)) {
            echo "Plaats niet ingevuld";
        } else if
        (empty($email)) {
            echo "E-mail niet ingevuld";
        } else if (empty($vakken)) {
            echo "Vakken niet ingevuld";
        }
    };
?>

<h1>lab 08 | Verwerken</h1>

<p><?= $voornaam ?></p>
<p><?= $achternaam ?></p>
<p><?= $straat ?></p>
<p><?= $postcode ?></p>
<p><?= $plaats ?></p>
<p><?= $email ?></p>


<p>
    <?php

    $vakken = $_POST['vakken'];
    if ($vakken == "I") {
        echo "Opleidingen ICT zijn vol. Kies een andere opleiding";
    } elseif ($vakken == "E") {
        echo "U wordt ingeschreven voor de volgende opleiding: Engels";
    } elseif ($vakken == "T") {
        echo "U wordt ingeschreven voor de volgende opleiding: Techniek";
    } elseif ($vakken == "N") {
        echo "U wordt ingeschreven voor de volgende opleiding: Nederlands";
    } else {
        echo "Vakken niet ingevuld";
    }

    ?>
</p>
</body>
</html>