<?php

function reiskosten($vertrek, $bestemming)
{

    // 1: amsterdam
    // 2: utrecht
    // 3: denhaag
    // 4: Rotterdam

    $reiskosten = array();

    $reiskosten[1] = array();
    $reiskosten[2] = array();
    $reiskosten[3] = array();
    $reiskosten[4] = array();

    $reiskosten[1][1] = 0;
    $reiskosten[1][2] = 30;
    $reiskosten[1][3] = 60;
    $reiskosten[1][4] = 90;

    $reiskosten[2][1] = 30;
    $reiskosten[2][2] = 0;
    $reiskosten[2][3] = 40;
    $reiskosten[2][4] = 20;

    $reiskosten[3][1] = 60;
    $reiskosten[3][2] = 40;
    $reiskosten[3][3] = 0;
    $reiskosten[3][4] = 10;

    $reiskosten[4][1] = 90;
    $reiskosten[4][2] = 20;
    $reiskosten[4][3] = 10;
    $reiskosten[4][4] = 0;


    return ($reiskosten[$vertrek][$bestemming]);
}


if (isset($_POST['vertrek'], $_POST['bestemming'])) {
    $vertrek = $_POST['vertrek'];
    $bestemming = $_POST['bestemming'];
    $reiskosten = reiskosten($vertrek, $bestemming);

}

// 1: amsterdam
// 2: utrecht
// 3: denhaag
// 4: Rotterdam


//switch ($reiskosten) {
//
//    case ($vertrek == "rotterdam" && $bestemming == "rotterdam"):
//    case ($vertrek == "denhaag" && $bestemming == "denhaag"):
//    case ($vertrek == "utrecht" && $bestemming == "utrecht"):
//    case ($vertrek == "amsterdam" && $bestemming == "amsterdam"):
//        echo "0 euro reiskosten";
//        break;
//
//    case ($vertrek == "utrecht" && $bestemming == "amsterdam"):
//    case ($vertrek == "amsterdam" && $bestemming == "utrecht"):
//        echo "30 euro reiskosten";
//        break;
//
//    case ($vertrek == "denhaag" && $bestemming == "amsterdam"):
//    case ($vertrek == "amsterdam" && $bestemming == "denhaag"):
//        echo "60 euro reiskosten";
//        break;
//
//    case ($vertrek == "rotterdam" && $bestemming == "amsterdam"):
//    case ($vertrek == "utrecht" && $bestemming == "rotterdam"):
//    case ($vertrek == "amsterdam" && $bestemming == "rotterdam"):
//        echo "90 euro reiskosten";
//        break;
//
//    case ($vertrek == "denhaag" && $bestemming == "utrecht"):
//    case ($vertrek == "utrecht" && $bestemming == "denhaag"):
//        echo "40 euro reiskosten";
//        break;
//
//    case ($vertrek == "rotterdam" && $bestemming == "denhaag"):
//    case ($vertrek == "denhaag" && $bestemming == "rotterdam"):
//        echo "10 euro reiskosten";
//        break;
//
//    case ($vertrek == "rotterdam" && $bestemming == "utrecht"):
//        echo "20 euro reiskosten";
//        break;
//
//
//    default:
//        echo "Geen vertrek ingevoerd";
//}


switch ($vertrek . $bestemming) {

    case ($vertrek == "rotterdam" && $bestemming == "rotterdam"):
    case ($vertrek == "denhaag" && $bestemming == "denhaag"):
    case ($vertrek == "utrecht" && $bestemming == "utrecht"):
    case ($vertrek == "amsterdam" && $bestemming == "amsterdam"):
        echo "0 euro reiskosten";
        break;

    case ($vertrek == "utrecht" && $bestemming == "amsterdam"):
    case ($vertrek == "amsterdam" && $bestemming == "utrecht"):
        echo "30 euro reiskosten";
        break;

    case ($vertrek == "denhaag" && $bestemming == "amsterdam"):
    case ($vertrek == "amsterdam" && $bestemming == "denhaag"):
        echo "60 euro reiskosten";
        break;

    case ($vertrek == "rotterdam" && $bestemming == "amsterdam"):
    case ($vertrek == "utrecht" && $bestemming == "rotterdam"):
    case ($vertrek == "amsterdam" && $bestemming == "rotterdam"):
        echo "90 euro reiskosten";
        break;

    case ($vertrek == "denhaag" && $bestemming == "utrecht"):
    case ($vertrek == "utrecht" && $bestemming == "denhaag"):
        echo "40 euro reiskosten";
        break;

    case ($vertrek == "rotterdam" && $bestemming == "denhaag"):
    case ($vertrek == "denhaag" && $bestemming == "rotterdam"):
        echo "10 euro reiskosten";
        break;

    case ($vertrek == "rotterdam" && $bestemming == "utrecht"):
        echo "20 euro reiskosten";
        break;


    default:
        echo "Geen vertrek ingevoerd";
}

?>

<h4>Reiskosten berekenen</h4>

<form method="post" action="">
    <label> Vertrek
        <select name="vertrek">
            <option value="amsterdam">Amsterdam</option>
            <option value="utrecht">Utrecht</option>
            <option value="denhaag">Den Haag</option>
            <option value="rotterdam">Rotterdam</option>
        </select>
    </label>


    <label> Bestemming
        <select name="bestemming">
            <option value="amsterdam">Amsterdam</option>
            <option value="utrecht">Utrecht</option>
            <option value="denhaag">Den Haag</option>
            <option value="rotterdam">Rotterdam</option>
        </select>
    </label>
    <button type="submit">Verzenden</button>
</form>

