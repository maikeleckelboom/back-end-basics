<?php

$brief = "
    Beste <b> <abonnee> </b>
    U heeft het laatste nummer van ons magazine ontvangen. <br>
    Omdat we u heel graag als abbonee willen behouden, bieden we u een aantrekkeljke en exclusieve korting:
    <br>U betaalt <b> <bedrag-met-korting> </b>
    in plaats van 65 euro. <br><br>
    <em>Profituur nu van deze aanbieding!</em><br><br>
    Met vriendelijke groet, <br>
    Sam Simons<br>
    Hoofdredacteur<br>.
";

// Onbewerkte versie
//echo $brief;

// Versie 2
$brief = str_replace("<bedrag-met-korting>", 50 , $brief);

// Versie 3
$brief = str_replace("<abonnee>", "Jan David", $brief);

echo $brief;
