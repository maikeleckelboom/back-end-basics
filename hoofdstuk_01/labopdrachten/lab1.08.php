<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../css/app.css">
    <title>Backend Basics | $_POST-variabelen</title>
    <style>

        * {
            box-sizing: border-box;
        }

        h1 {
            text-align: center;
            margin: 24px 0;
        }

        form {
            max-width: 400px;
            margin: 0 auto;
        }

        form input, form label {
            height: 38px;
            margin: 4px 0;
            font-size: 16px;
        }

        form input {
            padding: 8px;
        }

        form input[type="radio"] {
            margin: 4px;
            cursor: pointer;
        }

        form label {
            font-size: 18px;
            line-height: 2;
            position: relative;
            top: -14px;
            margin-right: 8px;
            cursor: pointer;
        }

        .left-side, .right-side {
            width: 200px;
            display: flex;
            flex-direction: column;
        }

        .sides {
            display: flex;
        }

        .buttons {
            justify-content: flex-end;
            display: flex;
        }

        .buttons input[type="submit"],
        .buttons input[type="reset"] {
            padding: 12px 18px;
            cursor: pointer;
            border-radius: 2px;
            border: solid 1px #cdcdcd;
            line-height: 0;
        }

        .buttons input[type="submit"] {
            background: #2a49a5;
            color: white;
        }

        .buttons input[type="reset"] {
            background: #88d4ff;
        }

        .buttons input:nth-child(1) {
            margin-right: 8px;
        }

    </style>
</head>
<body>

<h1>lab 08</h1>

<form action="lab1.08.verwerken.php" method="post">
    <div class="sides">
        <div class="left-side">
            <label for="voornaam">Voornaam</label>
            <label for="achternaam">Achternaam</label>
            <label for="straat">Straat</label>
            <label for="postcode">Postcode</label>
            <label for="plaats">Plaats</label>
            <label for="e-mail">E-mailadres</label>
        </div>
        <div class="right-side">
            <input type="text" name="voornaam" id="voornaam" placeholder="Voornaam" title="">
            <input type="text" name="achternaam" id="achternaam" placeholder="Achternaam" title="">
            <input type="text" name="straat" id="straat" placeholder="Straat" title="">
            <input type="text" name="postcode" id="postcode" placeholder="Postcode" title="">
            <input type="text" name="plaats" id="plaats" placeholder="Plaats" title="">
            <input type="email" name="e-mail" id="e-mail" placeholder="E-mailadres" title="">
        </div>
    </div>

    <div class="bottom">
        <h3>Kies een opleiding</h3>

        <input type="radio" name="vakken" value="I" title="" id="ict">
        <label for="ict">ICT</label>

        <input type="radio" name="vakken" value="E" title="" id="engels">
        <label for="engels">Engels</label>

        <input type="radio" name="vakken" value="T" title="" id="techniek">
        <label for="techniek">Techniek</label>

        <input type="radio" name="vakken" value="N" title="" id="nederlands">
        <label for="nederlands">Nederlands</label>

        <div class="buttons">
            <input type="reset" name="reset" value="Reset">
            <input type="submit" name="submit" value="Versturen">
        </div>
    </div>
</form>


</body>
</html>