<!DOCTYPE html>
<html lang="nl">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../css/app.css">
    <title>Voorbeeld van de do-while-lus</title>
</head>
<body>
<h3>Voorbeeld van de do-while-lus</h3>
<?php

echo "<br>Reken het saldo uit zolang saldo lager dan 2000 is";
$saldo = 100;
$rente = 0.1;
$maand = 1;

echo "<br>Beginsaldo is: $saldo";
echo "<br>START...";


do {

    $saldo += ($saldo * $rente);
    $saldo = sprintf("%0.2f", $saldo);


    echo "<br>Maand: $maand je saldo is: $saldo";

    $maand++;

    if ($maand == 2) {
        echo "<br>Februari betaald geen rente";
        continue;
    }

    echo "<br>Maximale saldo 2000 is bereikt";
} while ($maand == 6 && $saldo < 1000);

echo "<br>FINISH...";



//
//$i = 10;
//while (--$i)
//{
//    if ($i == 8)
//    {
//        continue;
//    }
//    if ($i == 5)
//    {
//        break;
//    }
//    echo $i . "\n";
//}




?>
<script src="../../js/ripple.js"></script>
</body>
</html>