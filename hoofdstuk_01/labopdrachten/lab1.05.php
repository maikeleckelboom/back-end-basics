<?php

// Opdrachten
// 1. Codeer de array ("een", "Twee", 3, "Vier")
// 2. Voeg het vijfde element met de waarde 5 eraan toe
// 3. Toon de array
// 4. Toon het datatype van het vijfde element
// 5. Verwijder het vijfde element
// 6. Toon de array
// 7. Voeg aan het begin een element in met de waarde nul
// 8. Toon de array
// 9. Delete index[0]
// 10. Toon het datatype van index[0]
// 11. Verwijder het eerste element
// 12. Toon de array
// 13. Verwijder het tweede element
// 14. Toon de array
// 15. Als index[2] bestaat: verwijder deze
// 16. Toon de array.

echo "<strong>Opdrachten 1/15</strong><br><br>";

// 1. Codeer de array ("een", "Twee", 3, "Vier")
$arrayOpdracht = array(
    $arrayOpdracht[0] = "één",
    $arrayOpdracht[1] = "twee",
    $arrayOpdracht[2] = 3,
    $arrayOpdracht[3] = "Vier"
);


$imploded_arr = implode(" ", $arrayOpdracht);

// 2. Voeg het vijfde element met de waarde 5 eraan toe
array_push($arrayOpdracht, 5);

// 3. Toon de array
echo "Stap  3: ";
echo implode(" ", $arrayOpdracht) . "<br>";
echo "<br>";


// 4. Toon het datatype van het vijfde element
echo "Stap 4: ";
echo gettype($arrayOpdracht[2]);
echo "<br>";
echo '<br>';

// 5. Verwijder het vijfde element
echo "Stap 6: ";
array_pop($arrayOpdracht);
echo implode(" ", $arrayOpdracht) . "<br>";
echo '<br>';


// 7. Voeg aan het begin een element in met de waarde nul
array_unshift($arrayOpdracht, "null");

// 8. Toon de array
echo "Stap 8: ";
echo implode(" ", $arrayOpdracht) . "<br>";
echo '<br>';


// 9. Delete index[0]
array_pop($arrayOpdracht);


// 10. Toon het datatype van index[0]
echo "Stap 10: ";
echo gettype($arrayOpdracht[0]);
echo "<br>";


// 11. Verwijder het eerste element
array_shift($arrayOpdracht);
echo '<br>';

// 12. Toon de array
echo "Opdracht 11: ";
echo implode(" ",$arrayOpdracht) . "<br>";
echo '<br>';


echo "Opdracht 13/14: ";
// 13. Verwijder het tweede element
unset($arrayOpdracht);
echo '<br>';


// 14. Toon de array
echo implode(" ",$arrayOpdracht) . "<br>";
echo '<br>';


echo "Opdracht 15/16: ";

// 15. Als index[2] bestaat: verwijder deze
$ifKeyExists = array_key_exists(2, $arrayOpdracht);
unset($arrayOpdracht[2]);
echo '<br>';


// 16. Toon de array
echo implode(" ",$arrayOpdracht) . "<br>";
echo '<br>';

