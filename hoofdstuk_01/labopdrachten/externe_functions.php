<?php


function korting()
{
    $korting = 0;
    if (isset($_POST['student'])) $korting =+ 15;
    if (isset($_POST['klant'])) $korting =+ 10;
    return ($korting);
}


function serviceKosten() {
    $serviceKosten = 0;
    $betalingswijze = $_POST['betalingswijze'];

    switch ($betalingswijze) {
        case "visa" :
            $serviceKosten = 2;
            break;
        case "mastercard" :
            $serviceKosten = 2.5;
            break;
        case "paypal" :
            $serviceKosten = 1.5;
            break;
        case "ideal" :
            $serviceKosten = 1;
            break;
        default:
            echo "Nog geen betaalmethode gekozen";
    }

    return number_format($serviceKosten, 2);
}



// Labopdracht 15
function facturering()
{

    echo "
         <tr>
            <th>Genre</th>
            <th>Artiest</th>
            <th>Album</th>
            <th>Aantal</th>
            <th>Prijs</th>
            <th>Bedrag</th>
        </tr>
    ";

    $albumcode = $_POST["albumcode"];
    $artiest = $_POST["artiest"];
    $album = $_POST["album"];
    $prijs = $_POST["prijs"];
    $genre = $_POST["genre"];
    $aantal = $_POST["aantal"];


    for ($i = 0; $i < sizeof($albumcode); $i++) {
        echo "<tr>";
        echo "<td>$genre[$i]</td>";
        echo "<td>$artiest[$i]</td>";
        echo "<td>$album[$i]</td>";
        echo "<td>$aantal[$i]</td>";
        echo "<td>€ $prijs[$i]</td>";
        echo "<td> € " . $bedrag[] = $prijs[$i] * $aantal[$i] . " </td>";
        echo "</tr>";
    }


    $totaalBedrag = array_sum($bedrag);
    $kortingsPercentage = korting(); 
    $serviceKosten = serviceKosten();
    $kortingsBedrag = ($kortingsPercentage / 100) * $totaalBedrag;
    $factuurTotaal = ($totaalBedrag - $kortingsBedrag) + $serviceKosten;

    echo "
        <tr>
            <td>Totaal</td>
            <td>&nbsp</td>
            <td>&nbsp</td>
            <td>&nbsp</td>
            <td>&nbsp</td>
            <td>
               € $totaalBedrag
            </td>
        </tr>";
    echo "
        <tr>
            <td>Korting</td>
            <td>&nbsp</td>
            <td>&nbsp</td>
            <td>&nbsp</td>
            <td>&nbsp</td>
            <td>";
    echo " € " . number_format($kortingsBedrag, 2) . "</td>";
    echo "</td>
       </tr>";

    echo "
        <tr>
            <td>Servicekosten</td>
            <td>&nbsp</td>
            <td>&nbsp</td>
            <td>&nbsp</td>
            <td>&nbsp</td>
            <td>";
    echo "€ " . $serviceKosten;
    echo "</td>
        </tr>";

    echo "
        <tr>
            <td>Te betalen</td>
            <td>&nbsp</td>
            <td>&nbsp</td>
            <td>&nbsp</td>
            <td>&nbsp</td>
            <td>";
    echo "€ " . number_format($factuurTotaal, 2);
    echo " </td>
        </tr>";
}


// Labopdracht 16
function premium()
{

    $aantallen = array_sum($_POST["aantal"]);

    if ($aantallen > 5) {
        $message = "<p class='mess_success'>Word lid van onze premium club.</p>";
    } else {
        $message = "<p class='mess_warning'>Je komt niet in aanmerking voor onze premium club.</p>";
    }

    echo $message;

}