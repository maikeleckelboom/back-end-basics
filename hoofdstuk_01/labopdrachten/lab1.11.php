<?php

$prijs = 9;

if (isset($_POST["submit"])) {
    $aantal = $_POST["aantal"];
    $aantal = implode(" ", $aantal);
    $totaal = $aantal * $prijs;
    $payment_method = $_POST['payment_method'];
}


if (isset($_POST["klant"], $_POST["student"])) {
    $kortingspercentage = ($totaal / 100) * 25;
    $resultaat = $totaal - $kortingspercentage . "<br>";

} elseif (isset($_POST["student"])) {
    $kortingspercentage = ($totaal / 100) * 15;
    $resultaat = $totaal - $kortingspercentage . "<br>";

} elseif (isset($_POST["klant"])) {
    $kortingspercentage = ($totaal / 100) * 10;
    $resultaat = $totaal - $kortingspercentage . "<br>";
}


?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Inconsolata:400,700&display=swap');

        body {
            font-size: 16px;
            font-family: 'Inconsolata', monospace;
        }

        .album {
            clear: left;
            width: 100%;
        }

        .omslag {
            float: left;
        }

        .gegevens {
            float: left;
            padding-left: 20px;
        }

        .korting {
            clear: left;
        }

        .aantal {
            background: #f8ce6c;
        }

        img {
            height: 72px;
        }

        .winkelmand-container {
            max-width: 600px;
            margin: 0 auto;
            margin-top: 200px;
            background: rgba(228, 228, 228, 0.24);
            padding: 20px;
        }

    </style>
    <title>Winkelmandje</title>
</head>
<body>

<div class="winkelmand-container">
    <h3>Mijn Winkelmandje</h3>

    <form name="albums" action="" method="post">

        <div class="album">
            <div class="omslag">
                <img src="../../files/shopping-cart.svg" alt="">
            </div>
            <div class="gegevens">
                <p>Cesaria Evora "Em Um Concerto"</p>

                <p>
                    <?php

                    // Prijs per stuk
                    echo "Prijs per stuk is €" . $prijs . "<br>";

                    // Totaal excl. korting
                    if (isset($totaal)) {
                        echo "Prijs totaal excl. korting is: €" . $totaal . "<br>";
                    } else {
                        echo "&nbsp";
                    }

                    // Totaal incl. korting
                    if (isset($resultaat)) {
                        echo "Totaal bedrag incl korting is: €" . $resultaat . "<br>";
                    } else {
                        echo "&nbsp";
                    }
                    // Totaal aantal producten
                    if (isset($aantal)) {
                        echo "Het aantal items is: " . $aantal . "<br>";
                    } else {
                        echo "&nbsp";
                    }

                    // Switch case
                    if (isset($payment_method)) {
                        switch ($_POST['payment_method']) {
                            case "visa" :
                                $method = "Visa";
                                break;
                            case "mastercard" :
                                $method = "MasterCard";
                                break;
                            case "paypal" :
                                $method = "PayPal";
                                break;
                            case "ideal" :
                                $method = "Ideal";
                                break;
                            default:
                                echo "Nog geen betaalmethode gekozen";
                        }
                        echo "Betaal methode: " . $method;
                    } else {
                        echo "Nog geen betaalmethode gekozen";
                    }

                    ?>
                </p>
                <input type="hidden" name="albumcode[0]" value="001">
                <input type="hidden" name="artiest[0]" value="Cesaria Evora">
                <input type="hidden" name="album[0]" value="Em Um Concerto">
                <input type="hidden" name="prijs[0]" value="9">
                <input type="hidden" name="genre[0]" value="World"> <br>
                <input type="text" size="2" maxlength="3" name="aantal[0]" class="aantal" value="0" title="">
            </div>
        </div>


        <div class="korting">
            <br>
            <hr>

            <p>Selecteer een betalingswijze</p>
            <label>
                <select name="payment_method">
                    <option value="visa">Visa</option>
                    <option value="mastercard">MasterCard</option>
                    <option value="paypal">PayPal</option>
                    <option value="ideal">Ideal</option>
                </select>
            </label>

            <p>Korting</p>
            <input type="checkbox" name="student" value="15" title=""> Student 15% <br>
            <input type="checkbox" name="klant" value="10" title=""> Klant 10% <br>
            <input type="submit" width="300px" name="submit" value="Bestellen">
        </div>

    </form>
</div>

</body>
</html>