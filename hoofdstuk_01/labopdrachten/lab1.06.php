<?php

// Stap 1
$playlist = array(
    array("genre" => "Hiphop", "ateur" => "John Williams", "titel" => "My Girl"),
    array("genre" => "Jazz", "ateur" => "John Coltrane", "Titel" => "New York"),
    array("genre" => "Hiphop", "ateur" => "Shakira", "Titel" => "Obsession")
);


// Stap 2
function printArray($item, $key)
{
    echo "<br> $key" . ":" . " <em>$item</em>";
}


// Stap 3
$addArray = array(array("genre" => "latin", "ateur" => "Caetano Veloso", "titel" => "Cafe Atlantic"));
$playlist = array_merge($playlist, $addArray);
array_walk_recursive($playlist, "printArray");
echo "<br>";


// Implode = array to string
// Explode = string to array

// Stap 4
function printTracks($item, $key)
{
    $track = implode(' | ', $item);
    echo "<br> Track: $key" . " : " . " $track";
}

print_r($playlist);

// Loop door de array playlist gebruik de functie printTracks na elke doorloop
array_walk($playlist, 'printTracks');