<!DOCTYPE html>
<html lang="nl">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../css/winkelmandje.css">
    <title>Winkelmandje</title>
</head>
<body>

<div class="winkelmand-container">
    <h3>Mijn Winkelmandje</h3>

    <form name="albums" action="" method="post">

        <div class="album">
            <div class="omslag">
                <img src="../../files/album1.jpg" alt="">
            </div>
            <div class="gegevens">
                <h4>Cesaria Evora "Em Um Concerto"</h4>
                <p>Prijs: €9.-</p>

                <input type="hidden" name="albumcode[0]" value="001">
                <input type="hidden" name="artiest[0]" value="Cesaria Evora">
                <input type="hidden" name="album[0]" value="Em Um Concerto">
                <input type="hidden" name="prijs[0]" value="9">
                <input type="hidden" name="genre[0]" value="World"> <br>
                <label for="aantal">Aantal:</label>
                <input type="number" size="2" maxlength="3" name="aantal[0]" id="aantal" value="0" title="">
            </div>
        </div>


        <div class="korting">


            <label>
                <span>Selecteer een betalingswijze:</span>
                <select name="betalingswijze">
                    <option value="visa">Visa</option>
                    <option value="mastercard">MasterCard</option>
                    <option value="paypal">PayPal</option>
                    <option value="ideal">Ideal</option>
                </select>
            </label>

            <p>Korting:</p>
            <input type="checkbox" name="student" value="15" title="" id="student">
            <label for="student">Student 15%</label> <br>
            <input type="checkbox" id="klant" name="klant" value="10" title="">
            <label for="klant">Klant 10%</label>
            <input type="submit" width="300px" name="verzenden" value="Bestellen">
        </div>

    </form>

    <div class="callout">
        <?php

        // Totaal excl. korting
        //                    if (isset($totaal)) {
        //                        echo "Prijs totaal excl. korting is: €" . $totaal . "<br>";
        //                    } else {
        //                        echo "&nbsp";
        //                    }
        // Totaal incl. korting
        //                    if (isset($resultaat)) {
        //                        echo "Totaal bedrag incl korting is: €" . $resultaat . "<br>";
        //                    } else {
        //                        echo "&nbsp";
        //                    }

        if (isset($_POST['verzenden'])) {
            include_once("externe_functions.php");
            $betalingswijze = $_POST['betalingswijze'];
            $serviceKosten = serviceKosten($betalingswijze);
            $aantal = $_POST["aantal"];
            $aantal = implode(" ", $aantal);
        }


        // Totaal aantal producten
        if (isset($aantal)) {
            echo "Het aantal is: " . $aantal . "<br>";
        } else {
            echo "&nbsp";
        }




        // Kortings percentage
        if (isset($_POST['verzenden'])) {
            echo "De korting is: " . korting() . "% <br>";
        }


        // Switch case
        if (isset($betalingswijze)) {
            switch ($_POST['betalingswijze']) {
                case "visa" :
                    $method = "Visa";
                    break;
                case "mastercard" :
                    $method = "MasterCard";
                    break;
                case "paypal" :
                    $method = "PayPal";
                    break;
                case "ideal" :
                    $method = "Ideal";
                    break;
                default:
                    echo "Nog geen betaalmethode gekozen";
            }
            echo "Betaal methode is: " . $method . "<br>";
        } else {
            echo "Nog geen betaalmethode gekozen";
        }


        ?>
    </div>

</div>

<script src="../../js/ripple.js"></script>
</body>
</html>