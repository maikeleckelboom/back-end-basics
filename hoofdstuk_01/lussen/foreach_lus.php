<!DOCTYPE html>
<html lang="nl">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../css/winkelmandje.css">
    <title>Voorbeeld foreach-lus</title>
</head>
<body>
<div class="foreach-container">
    <div class="callout">
        <h3>Voorbeeld foreach-lus</h3>
        <?php

        $kleuren["orangje"] = "orange";
        $kleuren["rood"] = "red";
        $kleuren["paars"] = "violet";
        $kleuren["groen"] = "green";
        $kleuren["blauw"] = "blue";
        $kleuren["geel"] = "yellow";

        foreach ($kleuren as $kleur) {
            if ($kleur == "yellow") {
                $kleur = "black";
            }

            echo "<br><Font color=$kleur>Deze tekst in $kleur";
        }

        ?>
    </div>
</div>
<script src="../../js/ripple.js"></script>
</body>
</html>