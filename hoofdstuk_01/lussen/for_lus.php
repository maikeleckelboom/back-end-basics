<!DOCTYPE html>
<html lang="nl">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <title>Backend Basics | for_lus</title>
</head>
<body>
<?php
echo "We willen de for-lus 10 rondjes laten lopen<br>";
echo "Klaar voor de START... <br>";
for($teller = 5; $teller <= 12; $teller ++) {
    echo "Dit is rondje $teller <br>";
}
echo "FINISH..";
?>
</body>
</html>