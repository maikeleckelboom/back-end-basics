<?php


echo "<h1>Arrays.php</h1>";
$producten = [];
$producten[0] = "Boeken";
$producten[1] = "CD's";
$producten[2] = "Smartphones";
$producten[3] = "DVD's";


// Sla producten over in de array met index nummer 3
echo "<h2>print_r()</h2>";
unset($producten[3]);
echo '<p>Print_r($producten)</p>';
print_r($producten);
echo "<br>";


echo "<h2>var_dump()</h2>";
// Snel de uitkomst zien van een variable
echo '<p>var_dump($producten)</p>';
var_dump($producten);
echo "<br>";


echo "<h2>Array_key_exists</h2>";
// Controleer of een bepaalde array-key (index) binnen een array bestaat.
// Return waarde i een boolean
echo "<p><strong>Opgave 9</strong></p>";
$gevonden = array_key_exists(1, $producten);
echo 'Key 1 gevonden? ' . $gevonden;
echo "<br>";


echo "<h2>in_array</h2>";
// Controleer of een bepaald array-element binnen een array bestaat
// Return waarde is een boolean
echo "<p><strong>Opgave 10</strong></p>";

$gevonden = in_array('Boeken', $producten);
echo 'Boeken gevonden? ' . $gevonden;
echo "<br>";


echo "<h2>array_search</h2>";
// Controleer of een bepaald array-element binnen een array bestaat
// Return waarde is het index nummer van het item in de array
echo "<p><strong>Opgave 11</strong></p>";
$index = array_search("CD's", $producten);
echo "De index van CD's is :" . $index;
echo "<br>";


echo "<h2>array_push</h2>";
// Voeg één of meer elementen toe aan de array
echo "<p><strong>Opgave 12</strong></p>";
array_push($producten, "Laptops", "Tablets");
print_r($producten);
echo "<br>";


echo "<h2>array_pop</h2>";
// Verwijdert en retourneert het laatste array-element
echo "<p><strong>Opgave 13</strong></p>";
$laatsteElement = array_pop($producten);
echo $laatsteElement . " is verwijderd " . "<br>";
print_r($producten);
echo "<br>";


echo "<h2>array_shift</h2>";
// Verwijdert en retourneert het eerte array-element
echo "<p><strong>Opgave 14</strong></p>";
$eersteElement = array_shift($producten);
echo $eersteElement . ' is Verwijderd ' . "<br>";
print_r($producten);
echo "<br>";


echo "<h2>array_unshift</h2>";
// Eén of meer elementen toevoegen aan het begin van een array
echo "<p><strong>Opgave 15</strong></p>";
array_unshift($producten, "TV's", "Stereo's");
echo "<p>TV's en Stereo's toegevoegd</p>";
print_r($producten);
echo "<br><br> ";


echo "<h2>array_rand</h2>";
// Eén of meer elementen toevoegen aan het begin van een array
echo "<p><strong>Opgave 16</strong></p>";
$random_keys = array_rand($producten, 2);
echo "<p>Eerste random product: </p> " . $producten[$random_keys[0]];
echo "<p> Tweede random product: </p>" . $producten[$random_keys[1]];

// Array doorlopen en waardes printen in de browser
function printArray($item, $key)
{
    echo "<br>$key" . ":" . " <i>$item</i>";
}

echo "<h2>array_walk</h2>";
echo "<p><strong>Opgave 17:</strong></p> Producten in array doorlopen";
array_walk($producten, 'printArray');
echo "<br>";



// Array doorlopen en waardes printen in de browser
$getallen = ["nul", "éen", "twee", "drie", "vier", "vijf"];
$tools = ["boek", "pen", "laptop", "potlood"];

echo "<h2>implode()</h2>";
$tekst1 = implode("*", $getallen);
echo "<p><strong>Opgave 18 <small>1/2</small>: </strong>Tekst1 in array1 converteren: $tekst1</p>";

$tekst2 = implode("|", $tools);
echo "<p><strong>Opgave 18 <small>2/2</small>: </strong>Tekst2 in array2 converteren $tekst2</p>";


echo "<h2>Explode()</h2>";
echo "<p><strong>Opgave 19 <small>1/2</small>: </strong>Tekst1 in array1 converteren: </p>";
$array1 = explode("*", $tekst1);
array_walk($array1, "printArray");

echo "<br>";

echo "<p><strong>Opgave 19 <small>2/2</small>: </strong>Tekst2 in array2 converteren </p>";
$array2 = explode("*", $tekst2);
array_walk($array2, "printArray");


echo "<h1>Associatieve Arrays</h1>";

$mijnArray = array("titel" => "Stoner", "ateur" => "John Williams");

print_r($mijnArray);


echo "<h1>Multi Dimensional Arrays</h1>";
echo "<h2>array_walk_recursive</h2>";

$boeken = array(
    array("titel" => "Stoner", "ateur" => "John Williams", "genre" => "Fictie", "prijs" => "19.99"),
    array("titel" => "De Circel", "ateur" => "Dave Eggers", "genre" => "Fictie", "prijs" => "22.50"),
    array("titel" => "Rayuela", "ateur" => "Julio Cortazar", "genre" => "Fictie", "prijs" => "25.50")
);

echo "<p>Opgave 20: Boeken-array aangemaakt</p>";

echo "<p>Opgave 21: Boeken recursief doorlopen (array_walk_recursive)</p>";
array_walk_recursive($boeken, "printArray");


$nieuweBoeken = array(
    array("titel" => "Spijt", "ateur" => "Carry Slee", "genre" => "Fictie", "prijs" => "12.99"),
    array("titel" => "Debet", "ateur" => "Saskia Noort", "genre" => "Fictie", "prijs" => "33.50"),
);

echo "<p>Opgave 22: Twee arrays samenvoegen (array_merge)</p>";

$boeken = array_merge($boeken, $nieuweBoeken);
array_walk_recursive($boeken, "printArray");


echo "<p>Opgave 23: Array elementen kopieëren (array_slice)</p>";
$oudeboeken = array_slice($boeken, 0, 2);
array_walk_recursive($oudeboeken, "printArray");
echo "<br>";


