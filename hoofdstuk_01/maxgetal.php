<?php

// Sla POST gegevens op in een variable
$input1 = $_POST["eerstegetal"];
$input2 = $_POST["tweedegetal"];

$getal1 = doubleval($_POST['eerstegetal']);
$getal2 = doubleval($_POST['tweedegetal']);

if (is_numeric($input1)) {
    echo "Ja, Dit is een getal <br>";
} elseif (is_numeric($input2)) {
    echo "Ja, Dit is een getal <br>";
} else {
    echo "Input moet een getal zijn<br>";
};


// Opgave 45
function maxGetal($getal1, $getal2)
{
    if ($getal1 > $getal2) {
        return ($getal1);
    } elseif ($getal2 > $getal1) {
        return ($getal2);
    } else {
        return ("Getallen zijn gelijk");
    }
}

if (isset($_POST["submit"])) {
    $maxgetal = maxGetal($_POST["eerstegetal"], $_POST["tweedegetal"]);
    echo $maxgetal;
}

?>
<form action="" method="post">
    <input type="text" name="eerstegetal" title="" placeholder="Eerste getal">
    <input type="text" name="tweedegetal" title="" placeholder="Tweede getal">
    <input type="submit" name="submit" value="Bereken hoogste getal" title="">
</form>

